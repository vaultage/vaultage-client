package org.vaultage.client.fs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.meta.IMeta;

import ch.marcsladek.commons.io.fs.FileCreator;

public class FSObjectMetaBuilderTest {

  private FSObjectMetaBuilder builder;

  private final Path TEST = Paths.get("test");
  private final Path DIR1 = TEST.resolve("dir1");
  private final Path DIR2 = TEST.resolve("dir2");
  private final Path DIR1_DIR3 = DIR1.resolve("dir3");
  private final Path DIR1_DIR3_FILE4 = DIR1_DIR3.resolve("file4");
  private final Path DIR1_FILE2 = DIR1.resolve("file2");
  private final Path DIR2_FILE3 = DIR2.resolve("file3");
  private final Path DIR1_FILE1 = DIR1.resolve("file1");

  private final String CONTENT_FILE1 = "This is file1";
  private final String CONTENT_FILE2 = "This is file2";
  private final String CONTENT_FILE3 = "This is file3";
  private final String CONTENT_FILE4 = "This is file4";

  @Before
  public void setUp() throws IOException {
    builder = new FSObjectMetaBuilder();
    createTest();
  }

  // TODO test errors

  @Test
  public void test_build() throws Exception {
    FSObject<IMeta> fsObj = builder.build(TEST);

    assertNotNull(fsObj);
    assertEquals("test", fsObj.getRoot());
    Map<String, IMeta> metaMap = fsObj.getClonedMap();
    assertMetaFolder(metaMap.remove("dir1"), "dir1");
    assertMetaFolder(metaMap.remove("dir2"), "dir2");
    assertMetaFolder(metaMap.remove("dir1/dir3"), "dir1/dir3");
    assertMetaFile(metaMap.remove("dir1/file1"), "dir1/file1", DigestUtils.sha1Hex(CONTENT_FILE1));
    assertMetaFile(metaMap.remove("dir1/file2"), "dir1/file2", DigestUtils.sha1Hex(CONTENT_FILE2));
    assertMetaFile(metaMap.remove("dir2/file3"), "dir2/file3", DigestUtils.sha1Hex(CONTENT_FILE3));
    assertMetaFile(metaMap.remove("dir1/dir3/file4"), "dir1/dir3/file4",
        DigestUtils.sha1Hex(CONTENT_FILE4));
    assertEquals(0, metaMap.size());
  }

  @After
  public void breakDown() throws Exception {
    FileUtils.deleteDirectory(TEST.toFile());
  }

  private void assertMetaFolder(IMeta meta, String path) {
    assertNotNull(meta);
    assertEquals(path, meta.getPath());
    assertTrue(meta.isFolder());
    assertNull(meta.getHash());
  }

  private void assertMetaFile(IMeta meta, String path, String hash) {
    assertNotNull(meta);
    assertEquals(path, meta.getPath());
    assertFalse(meta.isFolder());
    assertEquals(hash, meta.getHash());
  }

  private void createTest() throws IOException {
    Files.createDirectories(TEST);
    Files.createDirectories(DIR1);
    Files.createDirectories(DIR2);
    Files.createDirectories(DIR1_DIR3);
    FileCreator.createFile(DIR1_FILE1, CONTENT_FILE1.getBytes());
    FileCreator.createFile(DIR1_FILE2, CONTENT_FILE2.getBytes());
    FileCreator.createFile(DIR2_FILE3, CONTENT_FILE3.getBytes());
    FileCreator.createFile(DIR1_DIR3_FILE4, CONTENT_FILE4.getBytes());
  }

}

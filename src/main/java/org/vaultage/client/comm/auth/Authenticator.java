package org.vaultage.client.comm.auth;

import java.io.IOException;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.client.OutputHandler;
import org.vaultage.shared.comm.auth.Auth;
import org.vaultage.shared.comm.auth.AuthChallenge;
import org.vaultage.shared.comm.auth.AuthRequest;
import org.vaultage.shared.comm.auth.AuthResponse;

import ch.marcsladek.commons.concurrent.Startable;

public class Authenticator {

  private static final Logger LOGGER = LoggerFactory.getLogger(Authenticator.class);

  private final OutputHandler outputHandler;
  private final Startable afterAuthStartable;
  private String password;
  private boolean sent;

  public Authenticator(OutputHandler outputHandler, Startable afterAuthStartable) {
    this.outputHandler = Objects.requireNonNull(outputHandler);
    this.afterAuthStartable = Objects.requireNonNull(afterAuthStartable);
    sent = false;
  }

  public void request(String username, String password) throws IOException {
    AuthRequest authRequest = new AuthRequest(username);
    this.password = password;
    outputHandler.handleAuthRequest(authRequest);
    LOGGER.info(authRequest + " sent, waiting for answer...");
    // TODO timeout thread
  }

  public void authenticate(AuthChallenge authChallenge) throws IOException, InvalidKeySpecException {
    if (password != null) {
      Auth auth = new Auth(password, authChallenge.getSalt(), authChallenge.getChallenge());
      outputHandler.handleAuth(auth);
      sent = true;
      LOGGER.info(auth + " sent, waiting for answer...");
      // TODO timeout thread
    } else {
      throw new IllegalStateException("AuthResponse object hasn't been sent yet");
    }
  }

  public void response(AuthResponse authResponse) throws Exception {
    if (sent) {
      if (authResponse == AuthResponse.SUCCESS) {
        LOGGER.info("Authentication successful");
        afterAuthStartable.start();
      } else {
        LOGGER.warn("Authentication failed");
        // TODO shutdown?
      }
    } else {
      throw new IllegalStateException("Auth object hasn't been sent yet");
    }
  }

}

package org.vaultage.client.comm;

import java.io.IOException;
import java.net.Socket;

import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionFactory;

public class ServerConnectionFactory implements ConnectionFactory {

  @Override
  public Connection newInstance(Socket socket) throws IOException {
    return new ServerConnection(socket);
  }

}

package org.vaultage.client.comm;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.client.Delegator;
import org.vaultage.shared.comm.command.ServerCommand;

import ch.marcsladek.commons.concurrent.Startable;
import ch.marcsladek.jrtnp.client.Client;

public class ConnectionHandler extends Client implements Startable {

  private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionHandler.class);

  public ConnectionHandler(String host, int port) throws ReflectiveOperationException, IOException {
    super(host, port, false, ServerConnectionFactory.class);
  }

  public void setStartable(Startable startable) {
    ((ServerConnection) server).setStartable(startable);
  }

  public void setDelegator(Delegator delegator) {
    ((ServerConnection) server).setDelegator(delegator);
  }

  @Override
  public void started() {
    LOGGER.info("ConnectionHandler started for " + server.toString());
  }

  @Override
  public void shuttedDown() {
    LOGGER.info("ConnectionHandler shutdown for " + server.toString());
  }

  @Override
  public boolean isRunning() {
    return server.isListening();
  }

  @Override
  public boolean isTerminated() {
    return !server.isListening();
  }

  public String getAddress() {
    return server.getAddress();
  }

  public String getLocalAddress() {
    return server.getLocalAddress();
  }

  public void send(ServerCommand<?> command) throws IOException {
    server.send(command);
  }

}

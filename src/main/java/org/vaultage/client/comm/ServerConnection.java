package org.vaultage.client.comm;

import java.io.IOException;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.client.Delegator;
import org.vaultage.shared.comm.command.ClientCommand;

import ch.marcsladek.commons.concurrent.Startable;
import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionException;

public class ServerConnection extends Connection {

  private static final Logger LOGGER = LoggerFactory.getLogger(ServerConnection.class);

  private Startable startable;
  private Delegator delegator;

  ServerConnection(Socket socket) throws IOException {
    super(socket);
  }

  void setStartable(Startable startable) {
    this.startable = startable;
  }

  public void setDelegator(Delegator delegator) {
    this.delegator = delegator;
  }

  @Override
  protected void process(Object obj) {
    checkDelegator();
    if (obj instanceof ClientCommand) {
      ClientCommand<?> command = (ClientCommand<?>) obj;
      if (command.isValid(getIdentifier(), getLocalAddress())) {
        delegator.call(command);
      } else {
        LOGGER.warn("Unable to validate command from '" + getIdentifier() + "': " + command);
      }
    } else {
      LOGGER.warn("Illegal object received from '" + getIdentifier() + "': " + obj);
    }
  }

  private void checkDelegator() {
    if (delegator == null) {
      throw new IllegalStateException("Delegator is null");
    }
  }

  @Override
  protected void finish(boolean closedRemotely, ConnectionException closeException) {
    LOGGER.trace("Close Exception for " + (closedRemotely ? "remote" : "non-remote") + " close",
        closeException);
    if ((startable != null) && closedRemotely) {
      LOGGER.debug("Shutdown invoked due to remote close");
      startable.shutdown();
    }
  }

}

package org.vaultage.client.sync;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.client.OutputHandler;
import org.vaultage.client.fs.FSObjectMetaBuilder;
import org.vaultage.shared.fs.FSFactory;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.FSObjectBuildException;
import org.vaultage.shared.fs.FileData;
import org.vaultage.shared.fs.meta.IMeta;
import org.vaultage.shared.fs.meta.MetaFile;

public class SyncSupervisor {

  private static final Logger LOGGER = LoggerFactory.getLogger(SyncSupervisor.class);

  private final Path workingDir;
  private final OutputHandler outputHandler;
  private final FSObjectMetaBuilder metaBuilder;

  private final AtomicBoolean running;

  public SyncSupervisor(Path workingDir, OutputHandler outputHandler) {
    this.workingDir = workingDir;
    this.outputHandler = outputHandler;
    this.metaBuilder = new FSObjectMetaBuilder();
    running = new AtomicBoolean(false);
  }

  public boolean isRunning() {
    return running.get();
  }

  public void startSync() throws FSObjectBuildException, IOException {
    if (!running.getAndSet(true)) {
      FSObject<IMeta> fsObj = metaBuilder.build(workingDir);
      outputHandler.handleFSObject(fsObj);
    } else {
      throw new IllegalStateException("Second start not implemented"); // TODO
    }
  }

  public void fileRequest(MetaFile metaFile) {
    if (running.get()) {
      try {
        FileData fileData = FSFactory.getData(workingDir, metaFile);
        outputHandler.handleFileData(fileData);
      } catch (IOException exc) {
        String msg = "Unable to get data from file system for '" + metaFile + "'";
        LOGGER.error(msg, exc);
        try {
          syncStop(msg);
        } catch (IOException exc2) {
          LOGGER.error("Error while sending", exc2);
          // TODO retry/reconnect
        }
      }
    } else {
      throw new IllegalStateException("SyncSupervisor not running");
    }
  }

  public void syncStop(String msg) throws IOException {
    if (running.getAndSet(false)) {
      outputHandler.handleSyncStop(msg);
    } else {
      throw new IllegalStateException("SyncSupervisor not running");
    }
  }

  public boolean syncEnd() {
    return running.getAndSet(false);
  }

}

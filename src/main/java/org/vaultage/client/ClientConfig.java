package org.vaultage.client;

import java.io.IOException;
import java.nio.file.Path;

import org.apache.commons.configuration.ConfigurationException;

import ch.marcsladek.commons.util.config.Config;
import ch.marcsladek.commons.util.config.Property;

public class ClientConfig extends Config {

  private static final Path CONFIG_FILE = Client.DIR.resolve("config.properties");

  public static final Property HOST = new Property("connection.host", "localhost");
  public static final Property PORT = new Property("connection.port", 12963);
  public static final Property WORKING_DIR = new Property("setup.working-directory",
      Client.USER_HOME.resolve("vaultage"));
  public static final Property SYNC_INTERVAL = new Property("sync.interval", 30);

  ClientConfig() throws ConfigurationException, IllegalAccessException, IOException {
    super(CONFIG_FILE, ClientConfig.class);
    this.createDirs();
  }

}

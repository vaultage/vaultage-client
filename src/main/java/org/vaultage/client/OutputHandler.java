package org.vaultage.client;

import java.io.IOException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.client.comm.ConnectionHandler;
import org.vaultage.shared.comm.auth.Auth;
import org.vaultage.shared.comm.auth.AuthRequest;
import org.vaultage.shared.comm.command.ServerCommand;
import org.vaultage.shared.comm.command.ServerCommandType;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.FileData;
import org.vaultage.shared.fs.meta.IMeta;

public final class OutputHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(OutputHandler.class);

  private final ConnectionHandler connHandler;
  private final String receiver;

  OutputHandler(ConnectionHandler connHandler) {
    this.connHandler = Objects.requireNonNull(connHandler);
    this.receiver = connHandler.getAddress();
  }

  public void handleAuthRequest(AuthRequest authRequest) throws IOException {
    send(new ServerCommand<AuthRequest>(ServerCommandType.AuthRequestCommand,
        connHandler.getLocalAddress(), receiver, authRequest));
  }

  public void handleAuth(Auth auth) throws IOException {
    send(new ServerCommand<Auth>(ServerCommandType.AuthCommand, connHandler.getLocalAddress(),
        receiver, auth));
  }

  public void handleFSObject(FSObject<IMeta> fsObj) throws IOException {
    send(new ServerCommand<FSObject<IMeta>>(ServerCommandType.SyncRequestCommand,
        connHandler.getLocalAddress(), receiver, fsObj));
  }

  public void handleFileData(FileData fileData) throws IOException {
    send(new ServerCommand<FileData>(ServerCommandType.FileSendCommand,
        connHandler.getLocalAddress(), receiver, fileData));
  }

  public void handleSyncStop(String message) throws IOException {
    send(new ServerCommand<String>(ServerCommandType.SyncStopCommand,
        connHandler.getLocalAddress(), receiver, message));
  }

  public void handleTest(Object obj) throws IOException {
    send(new ServerCommand<Object>(ServerCommandType.TestCommand, connHandler.getLocalAddress(),
        receiver, obj));
  }

  private void send(ServerCommand<?> command) throws IOException {
    connHandler.send(command);
    LOGGER.info("Sent '" + command + "'");
  }
}

package org.vaultage.client.fileHandler;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.client.sync.SyncSupervisor;
import org.vaultage.shared.fs.FSObjectBuildException;

public class DirectoryChangeHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryChangeHandler.class);

  private final SyncSupervisor syncSupervisor;

  public DirectoryChangeHandler(SyncSupervisor syncSupervisor) {
    this.syncSupervisor = syncSupervisor;
  }

  public boolean changed() {
    LOGGER.debug("Directory change invoked");
    try {
      syncSupervisor.startSync(); // TODO multiple start not working
      return true;
    } catch (FSObjectBuildException exc) {
      LOGGER.warn("Exception while building FSObject", exc);
      
    } catch (IOException exc) {
      LOGGER.warn("Exception while handling FSObject", exc);
    }
    return false;
  }

}

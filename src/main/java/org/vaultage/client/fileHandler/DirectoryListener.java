package org.vaultage.client.fileHandler;

import static java.nio.file.StandardWatchEventKinds.*;

import java.io.IOException;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.marcsladek.commons.concurrent.Startable;

public class DirectoryListener extends Thread implements Startable {

  private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryListener.class);

  private volatile boolean listening;
  private volatile boolean terminated;
  private final Path directory;
  private final DirectoryChangeHandler changeHandler;
  private final WatchService watcher;

  public DirectoryListener(Path directory, DirectoryChangeHandler handler)
      throws IOException {
    listening = false;
    terminated = false;
    this.directory = directory;
    this.changeHandler = handler;
    watcher = FileSystems.getDefault().newWatchService();
    directory.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
  }

  @Override
  public void run() {
    LOGGER.info("DirectoryListener started. Listening to '" + directory + "'");
    listening = true;
    try {
      boolean retry = true;
      while (listening) {
        if (retry || poll()) {
          retry = !changeHandler.changed();
        } else {
          LOGGER.debug("No changes for poll");
        }
      }
    } catch (ClosedWatchServiceException exc) {
      LOGGER.trace("WatchService closed");
    } catch (InterruptedException exc) {
      LOGGER.error("DirectoryListener interrupted unexpectedly", exc);
    }
    LOGGER.info("DirectoryListener shutdown");
    listening = false;
    terminated = true;
  }

  private boolean poll() throws InterruptedException, ClosedWatchServiceException {
    WatchKey key = watcher.take();
    return !key.pollEvents().isEmpty() && key.reset();
  }

  @Override
  public boolean isRunning() {
    return listening;
  }

  @Override
  public void shutdown() {
    LOGGER.debug("DirectoryListener shutdown call");
    listening = false;
    IOUtils.closeQuietly(watcher);
  }

  @Override
  public boolean isTerminated() {
    return terminated;
  }
}

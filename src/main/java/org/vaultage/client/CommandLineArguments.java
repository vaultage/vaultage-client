package org.vaultage.client;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

class CommandLineArguments {

  private static final Option USERNAME = new Option("u", true, "username");
  private static final Option PASSWORD = new Option("p", true, "password");

  private final CommandLine cmd;

  CommandLineArguments(String[] args) throws ParseException {
    Options options = new Options();
    options.addOption(USERNAME);
    options.addOption(PASSWORD);
    cmd = new PosixParser().parse(options, args);
    checkOption(USERNAME);
    checkOption(PASSWORD);
  }

  private void checkOption(Option option) throws ParseException {
    if (!cmd.hasOption(USERNAME.getOpt())) {
      throw new ParseException("Command line arguemnt missing: -" + option.getOpt() + " "
          + option.getDescription());
    }
  }

  String getUsername() {
    return cmd.getOptionValue(USERNAME.getOpt());
  }

  String getPassword() {
    return cmd.getOptionValue(PASSWORD.getOpt());
  }
}

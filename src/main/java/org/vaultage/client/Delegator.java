package org.vaultage.client;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.client.comm.auth.Authenticator;
import org.vaultage.client.sync.SyncSupervisor;
import org.vaultage.shared.comm.auth.AuthChallenge;
import org.vaultage.shared.comm.auth.AuthResponse;
import org.vaultage.shared.comm.command.ClientCommand;
import org.vaultage.shared.fs.meta.MetaFile;

import ch.marcsladek.commons.concurrent.Callable;

public class Delegator implements Callable<Void, ClientCommand<?>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(Delegator.class);

  private final Authenticator authenticator;
  private final SyncSupervisor syncSupervisor;

  public Delegator(Authenticator authenticator, SyncSupervisor syncSupervisor) {
    this.authenticator = Objects.requireNonNull(authenticator);
    this.syncSupervisor = Objects.requireNonNull(syncSupervisor);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Void call(ClientCommand<?> command) {
    LOGGER.info("Received '" + command + "'");
    switch (command.getType()) {
    case AuthChallengeCommand:
      delegateAuthChallengeCommand((ClientCommand<AuthChallenge>) command);
      break;
    case AuthResponseCommand:
      delegateAuthResponseCommand((ClientCommand<AuthResponse>) command);
      break;
    case FileRequestCommand:
      delegateFileRequestCommand((ClientCommand<MetaFile>) command);
      break;
    case SyncEndCommand:
      delegateSyncEndCommand((ClientCommand<Boolean>) command);
      break;
    case TestCommand:
      break;
    default:
      throw new IllegalArgumentException("Received invalid command '" + command + "'");
    }
    return null;
  }

  private void delegateAuthChallengeCommand(ClientCommand<AuthChallenge> command) {
    try {
      authenticator.authenticate(command.getData());
    } catch (Exception exc) {
      LOGGER.error("Error authenticating", exc);
      // TODO ???
    }
  }

  private void delegateAuthResponseCommand(ClientCommand<AuthResponse> command) {
    try {
      authenticator.response(command.getData());
    } catch (Exception exc) {
      LOGGER.error("Error starting afterAuthStartable", exc);
      // TODO ???
    }
  }

  private void delegateFileRequestCommand(ClientCommand<MetaFile> command) {
    syncSupervisor.fileRequest(command.getData());
  }

  private void delegateSyncEndCommand(ClientCommand<Boolean> command) {
    if (syncSupervisor.syncEnd()) {
      LOGGER.info("Sync ended successfully");
    } else {
      LOGGER.info("Sync ended forcefully");
    }
  }

}

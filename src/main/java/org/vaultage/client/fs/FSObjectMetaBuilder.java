package org.vaultage.client.fs;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.shared.fs.FSFactory;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.FSObjectBuildException;
import org.vaultage.shared.fs.meta.IMeta;

public class FSObjectMetaBuilder {

  private static final Logger LOGGER = LoggerFactory.getLogger(FSObjectMetaBuilder.class);

  /**
   * Creates an {@link FSObject} containing {@link IMeta} elements for the given root folder.
   * 
   * @param rootPath
   *          path of root folder which should be represented. May not be null.
   * @return built {@link FSObject}
   * @throws FSObjectBuildException
   *           when unable to read out files
   */
  public FSObject<IMeta> build(Path rootPath) throws FSObjectBuildException {
    LOGGER.debug("Creating FSObjectMeta for '" + rootPath + "'");
    try {
      FSObject<IMeta> fsObj = new FSObject<IMeta>(rootPath.toString());
      Files.walkFileTree(rootPath, new FSObjectBuilderVisitor(fsObj)); // TODO check if terminated
      LOGGER.trace("FSObjectMeta built '" + fsObj + "'");
      return fsObj;
    } catch (Exception exc) {
      throw new FSObjectBuildException("Building FSObjectMeta failed due to exception", exc);
    }
  }

  private class FSObjectBuilderVisitor extends SimpleFileVisitor<Path> {

    private final FSObject<IMeta> fsObj;
    private final Path rootPath;

    FSObjectBuilderVisitor(FSObject<IMeta> fsObj) {
      this.fsObj = fsObj;
      rootPath = Paths.get(fsObj.getRoot());
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
        throws IOException {
      FileVisitResult ret = FileVisitResult.TERMINATE;
      if (rootPath.equals(dir)) {
        ret = FileVisitResult.CONTINUE;
      } else {
        Path relPath = rootPath.relativize(dir);
        if (fsObj.put(relPath.toString(), FSFactory.getMeta(dir, relPath))) {
          ret = FileVisitResult.CONTINUE;
        }
      }
      return ret;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
      FileVisitResult ret = FileVisitResult.TERMINATE;
      Path relPath = rootPath.relativize(file);
      if (fsObj.put(relPath.toString(), FSFactory.getMeta(file, relPath))) {
        ret = FileVisitResult.CONTINUE;
      }
      return ret;
    }

  }

}

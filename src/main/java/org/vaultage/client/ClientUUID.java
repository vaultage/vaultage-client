package org.vaultage.client;

import java.nio.file.Path;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ClientUUID {

  private static final Logger LOGGER = LoggerFactory.getLogger(ClientUUID.class);

  private final Path path;
  private UUID uuidCache;

  ClientUUID(Path path) {
    this.path = path;
  }

  UUID get() {
    UUID uuid = uuidCache;
    if (uuid == null) {
      uuid = getUUIDFromFileSystem();
      if (uuid == null) {
        uuid = UUID.randomUUID();
        saveUUIDToFileSystem(uuid);
        LOGGER.debug("New ClientUUID generated");
      }
      uuidCache = uuid;
    }
    return uuid;
  }

  private UUID getUUIDFromFileSystem() {
    UUID uuid = null;
    try {
      byte[] byteArray = FileUtils.readFileToByteArray(path.toFile());
      uuid = (UUID) SerializationUtils.deserialize(byteArray);
      LOGGER.debug("Retrieved ClientUUID from file system");
    } catch (Exception exc) {
      LOGGER.info("Unable to retrieve ClientUUID from file system");
    }
    return uuid;
  }

  private void saveUUIDToFileSystem(UUID uuid) {
    try {
      byte[] byteArray = SerializationUtils.serialize(uuid);
      FileUtils.writeByteArrayToFile(path.toFile(), byteArray);
    } catch (Exception exc) {
      LOGGER.error("Unable to write ClientUUID to file");
    }
  }

}

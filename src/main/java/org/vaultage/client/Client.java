package org.vaultage.client;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.vaultage.client.comm.ConnectionHandler;
import org.vaultage.client.comm.auth.Authenticator;
import org.vaultage.client.fileHandler.DirectoryChangeHandler;
import org.vaultage.client.fileHandler.DirectoryListener;
import org.vaultage.client.sync.SyncSupervisor;

import ch.marcsladek.commons.concurrent.Startable;
import ch.marcsladek.commons.concurrent.Startup;
import ch.marcsladek.commons.io.ConsoleReader;
import ch.marcsladek.commons.util.config.Config;
import ch.marcsladek.commons.util.config.IllegalPropertyTypeException;

public class Client {

  public static final Path USER_HOME = Paths.get(System.getProperty("user.home"));
  public static final Path DIR = USER_HOME.resolve(".vaultage");

  public static final String FILE_RES = "resources.vlt";

  private static Startable startable;

  public static void main(String[] args) throws Exception {

    CommandLineArguments arguments = new CommandLineArguments(args);

    Config config = new ClientConfig();

    // XXX uuid gen for later usage
    new ClientUUID(DIR.resolve(FILE_RES)).get();

    ConsoleReader consoleReader = getConsoleReader();
    ConnectionHandler connHandler = getConnectionHandler(config);
    OutputHandler outputHandler = new OutputHandler(connHandler);
    SyncSupervisor syncSupervisor = new SyncSupervisor(config.getPath(ClientConfig.WORKING_DIR),
        outputHandler);
    DirectoryListener listener = getDirectoryListener(config, syncSupervisor);

    Startable beforeAuthStartable = new Startup(connHandler, consoleReader);
    Startable afterAuthStartable = new Startup(listener);
    startable = new Startup(Startup.CHECK_MODE_NONE, false, beforeAuthStartable, afterAuthStartable);

    Authenticator authenticator = new Authenticator(outputHandler, afterAuthStartable);
    Delegator delegator = new Delegator(authenticator, syncSupervisor);

    connHandler.setStartable(startable);
    connHandler.setDelegator(delegator);

    beforeAuthStartable.start();

    authenticator.request(arguments.getUsername(), arguments.getPassword());
  }

  private static ConsoleReader getConsoleReader() {
    return new ConsoleReader() {

      @Override
      protected void process(String line) {
        if (line.toLowerCase().contains("quit")) {
          startable.shutdown();
        } else {
          System.out.println("Type 'quit' for closing the client");
        }
      }
    };
  }

  private static ConnectionHandler getConnectionHandler(Config config)
      throws ReflectiveOperationException, IOException, IllegalPropertyTypeException {
    return new ConnectionHandler(config.getString(ClientConfig.HOST),
        config.getInt(ClientConfig.PORT));
  }

  private static DirectoryListener getDirectoryListener(Config config, SyncSupervisor syncSupervisor)
      throws IOException, IllegalPropertyTypeException {
    DirectoryChangeHandler changeHandler = new DirectoryChangeHandler(syncSupervisor);
    return new DirectoryListener(config.getPath(ClientConfig.WORKING_DIR), changeHandler);
  }

}
